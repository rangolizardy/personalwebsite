var controlparent = require('../lib/controlparent.js');
class Test extends controlparent{
    constructor(manager) {
        super(manager);
        this.name = "test";

        this.app.get('/test', function (req, res) {
            res.render('test');
        });
    }
}

module.exports = Test;
