//heeeyaaa
var walk = require('walk');
var express = require('express');
var exphbs  = require('express-handlebars');

var database = require('./lib/database.js');

var app = express();
app.use(express.static('public'));

app.engine('handlebars', exphbs({
                                  defaultLayout : 'main',
                                  extname       : '.handlebars',
                                }));
app.set('view engine', 'handlebars');

var manager = {};
manager.get = {};
manager.get.app = function(){return(app)}

// to load the controllers
files = [];
handlers = []; // handlers contain the handlers

//load controllers
loadControllerFiles('controllers',controllerOnReady);

function loadControllerFiles( directory, onready ){

        if(typeof onready !== "function"){
            onready = function(){return true}
        }
        var walker  = walk.walk('./'+directory, { followLinks: false });
        walker.on('file', function(root, stat, next) {
            // Add this file to the list of files
            if (stat.name.match('.js')) {
                files.push(root + '/' + stat.name);
            }
            next();
        });
        walker.on('end', function() {
            loadControllers(onready);
        });

}
function loadControllers(onready){
        for(i = 0; i < files.length; i++){
            try{
                var tmp = require(files[i]);
                initializeController(tmp);
            } catch(e){
                console.log(e);
                // do something, idk
            }
        }
        onready();
}

function initializeController(controller){
    var cntrl = new controller(manager);
    handlers[cntrl.name] = cntrl;
}

function controllerOnReady(){
    app.listen(3000, function () {
      console.log('Example app listening on port 3000!');
    });
}
