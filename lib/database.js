//database handler
class database {
    constructor(manager) {
        this.manager = manager;
        this.nosql = require('nosql').load('./db/database.nosql');
    }
}

module.exports = database;